#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

int main(){
	// struct to get time in second.
	struct timeval tv;
	
	// a string to save standard date format.
	char buffer[30];
	
	// getting seconds from 1970 to now via a syscall.
	gettimeofday(&tv, NULL);
	
	// converting seconds to standard date format.
	strftime (buffer, 30, "%Y-%m-%d   %H:%M:%S", localtime(&tv.tv_sec));
	
	// printing!
	printf("%s", buffer);
	
	// returning!!!
	return 0;
}
